package com.neosoft.springbootsecurity.dto;

import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ResponseResult {
	private List<String> errors;
	private String message;
	private int statusCode;
	public ResponseResult(List<String> errors, String message, int statusCode) {
		super();
		this.errors = errors;
		this.message = message;
		this.statusCode = statusCode;
	}
	

}
